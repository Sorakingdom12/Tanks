using Cinemachine;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Complete
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        #region VARIABLES

        public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game
        public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases
        public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases
        public CameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases
        public TextMeshProUGUI m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
        public GameObject m_TankPrefab;             // Reference to the prefab the players will control
        public TankManager[] m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks
        public Camera m_MainCamera;
        public Camera[] m_Cameras;
        public Camera m_Map;
        public GameObject m_Buttons;
        public PlayerInputManager m_PlayerInputManager;

        private int m_PlayerAmount;
        private int m_RemainingPlayers;
        private int m_RoundNumber;                  // Which round the game is currently on
        private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts
        private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends
        private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won
        private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won

        #endregion

        private void Awake()
        {
            if (instance != null)
                Destroy(instance.gameObject);
            instance = this;
        }

        private void Start()
        {
            m_StartWait = new WaitForSeconds(m_StartDelay);
            m_EndWait = new WaitForSeconds(m_EndDelay);
            ResetCameras();
            //StartCoroutine(WaitForMinPlayers());
        }

        public void SetPlayers(int num)
        {
            bool invalidDevice = false;
            for (int i = 0; i < InputSystem.devices.Count; i++)
            {
                if (InputSystem.devices[i].displayName == "Mouse")
                    invalidDevice = true;
            }

            if (InputSystem.devices.Count < num || (InputSystem.devices.Count == num && invalidDevice))
            {
                Debug.Log("Not Enough Devices");
                return;
            }

            for (int i = 0; i < num; i++)
            {
                if (i == 0) m_PlayerInputManager.JoinPlayer(i, -1, null, InputSystem.devices[0]);
                else m_PlayerInputManager.JoinPlayer(i, -1, null, InputSystem.devices[i + 1]);
            }
            m_Buttons.SetActive(false);
            Debug.Log("Starting players joined! Amount: " + m_PlayerInputManager.playerCount);
            m_PlayerInputManager.joinBehavior = PlayerJoinBehavior.JoinPlayersWhenJoinActionIsTriggered;
            StartCoroutine(GameLoop());
        }

        public void SetupTank(int i)
        {
            m_Tanks[i].m_PlayerNumber = i + 1;
            m_Tanks[i].Setup();
            SetCamera(m_Tanks[i], i);
        }

        private void ResetCameras()
        {
            foreach (Camera camera in m_Cameras)
            {
                camera.gameObject.SetActive(false);
            }
        }

        private void SetCamera(TankManager tank, int i)
        {
            tank.m_Instance.gameObject.GetComponentInChildren<CinemachineFreeLook>().gameObject.layer = 11 + i;


            if (i < m_PlayerAmount)
                m_Cameras[i].gameObject.SetActive(true);
        }

        private void SetCameraTargets()
        {
            // Create a collection of transforms the same size as the number of tanks
            Transform[] targets = new Transform[m_PlayerAmount];

            // For each of these transforms...
            for (int i = 0; i < targets.Length; i++)
            {
                // ... set it to the appropriate tank transform
                targets[i] = m_Tanks[i].m_Instance.transform;
            }

            // These are the targets the camera should follow
            m_CameraControl.m_Targets = targets;
        }

        private void HandleCameras()
        {
            bool placedTop = false;
            for (int i = 0; i < m_PlayerAmount; ++i)
            {
                if (m_RemainingPlayers > 1)
                {
                    if (m_RemainingPlayers < 3 && !m_Tanks[i].IsDead())
                    {
                        m_Cameras[i].rect = new Rect(0.06f, (!placedTop) ? 0.53f : 0.03f, 0.88f, 0.44f);
                        placedTop = true;
                    }
                    else m_Cameras[i].rect = new Rect((i % 2 == 0) ? 0.03f : 0.53f, (i < 2) ? 0.53f : 0.03f, 0.444f, 0.444f);

                    // Set Map if 3 players 
                    if (m_RemainingPlayers != 3)
                        m_Map.gameObject.SetActive(false);
                    else
                        m_Map.gameObject.SetActive(true);
                    // Set Cameras up/down depending if tank is alive
                    if (m_Tanks[i].IsDead())
                        m_Cameras[i].gameObject.SetActive(false);
                    else
                        m_Cameras[i].gameObject.SetActive(true);
                }
                else
                {
                    m_MainCamera.gameObject.SetActive(true);
                    foreach (Camera cam in m_Cameras)
                        cam.gameObject.SetActive(false);
                    m_Map.gameObject.SetActive(false);
                }
            }
        }

        public void ToggleSplitCameras(bool value)
        {
            for (int i = 0; i < m_PlayerAmount; i++)
            {
                m_Cameras[i].gameObject.SetActive(value);
            }
            if (m_PlayerAmount == 3)
                m_Map.gameObject.SetActive(value);
        }

        #region GAME LOOP
        // This is called from start and will run each phase of the game one after another
        private IEnumerator GameLoop()
        {

            // Start off by running the 'RoundStarting' coroutine but don't return until it's finished
            yield return StartCoroutine(RoundStarting());

            // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished
            yield return StartCoroutine(RoundPlaying());

            // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished
            yield return StartCoroutine(RoundEnding());

            // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found
            if (m_GameWinner != null)
            {
                // If there is a game winner, restart the level
                SceneManager.LoadScene(0);
            }
            else
            {
                // If there isn't a winner yet, restart this coroutine so the loop continues
                // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end
                StartCoroutine(GameLoop());
            }
        }

        private IEnumerator RoundStarting()
        {
            // As soon as the round starts reset the tanks and make sure they can't move
            ResetAllTanks();
            // SET CAMERA VIEWPORTS
            HandleCameras();
            m_MainCamera.gameObject.SetActive(false);

            DisableTankControl();

            // Snap the camera's zoom and position to something appropriate for the reset tanks
            m_CameraControl.SetStartPositionAndSize();

            // Increment the round number and display text showing the players what round it is
            m_RoundNumber++;
            m_MessageText.text = "ROUND " + m_RoundNumber;

            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_StartWait;
        }

        private IEnumerator RoundPlaying()
        {
            // As soon as the round begins playing let the players control the tanks
            EnableTankControl();

            // Clear the text from the screen
            m_MessageText.text = string.Empty;

            // While there is not one tank left...
            while (!OneTankLeft())
            {
                // ... return on the next frame
                yield return null;
            }
        }

        private IEnumerator RoundEnding()
        {
            // Stop tanks from moving
            DisableTankControl();

            // Clear the winner from the previous round
            m_RoundWinner = null;

            // See if there is a winner now the round is over
            m_RoundWinner = GetRoundWinner();

            // If there is a winner, increment their score
            if (m_RoundWinner != null)
                m_RoundWinner.m_Wins++;

            // Now the winner's score has been incremented, see if someone has one the game
            m_GameWinner = GetGameWinner();

            // Get a message based on the scores and whether or not there is a game winner and display it
            string message = EndMessage();
            m_MessageText.text = message;

            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_EndWait;
        }

        // This is used to check if there is one or fewer tanks remaining and thus the round should end
        private bool OneTankLeft()
        {
            // Start the count of tanks left at zero
            int numTanksLeft = 0;

            // Go through all the tanks...
            for (int i = 0; i < m_PlayerAmount; i++)
            {
                // ... and if they are active, increment the counter
                if (m_Tanks[i].m_Instance.transform.GetChild(0).gameObject.activeSelf)
                    numTanksLeft++;
            }

            // If there are one or fewer tanks remaining return true, otherwise return false
            return numTanksLeft <= 1;
        }

        // This function is to find out if there is a winner of the round
        // This function is called with the assumption that 1 or fewer tanks are currently active
        private TankManager GetRoundWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if one of them is active, it is the winner so return it
                if (m_Tanks[i].m_Instance.transform.GetChild(0).gameObject.activeSelf)
                {
                    return m_Tanks[i];
                }
            }

            // If none of the tanks are active it is a draw so return null
            return null;
        }

        // This function is to find out if there is a winner of the game
        private TankManager GetGameWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if one of them has enough rounds to win the game, return it
                if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                {
                    return m_Tanks[i];
                }
            }

            // If no tanks have enough rounds to win, return null
            return null;
        }

        // Returns a string message to display at the end of each round.
        private string EndMessage()
        {
            // By default when a round ends there are no winners so the default end message is a draw
            string message = "DRAW!";

            // If there is a winner then change the message to reflect that
            if (m_RoundWinner != null)
            {
                message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";
            }

            // Add some line breaks after the initial message
            message += "\n\n\n\n";

            // Go through all the tanks and add each of their scores to the message
            for (int i = 0; i < m_PlayerAmount; i++)
            {
                message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
            }

            // If there is a game winner, change the entire message to reflect that
            if (m_GameWinner != null)
            {
                message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";
            }

            return message;
        }
        #endregion

        #region TANK MANAGEMENT
        // This function is used to turn all the tanks back on and reset their positions and properties
        private void ResetAllTanks()
        {
            for (int i = 0; i < m_PlayerAmount; i++)
            {
                m_Tanks[i].Reset();
            }
            m_RemainingPlayers = m_PlayerAmount;
            HandleCameras();
        }

        private void EnableTankControl()
        {
            for (int i = 0; i < m_PlayerAmount; i++)
            {
                m_Tanks[i].EnableControl();
            }
        }

        private void DisableTankControl()
        {
            for (int i = 0; i < m_PlayerAmount; i++)
                m_Tanks[i].DisableControl();
        }

        public void HandleTankDeath()
        {
            --m_RemainingPlayers;
            HandleCameras();
        }

        public void OnPlayerJoined(PlayerInput playerInput)
        {
            GameObject tank = playerInput.gameObject;
            m_Tanks[m_PlayerAmount].m_Instance = tank;
            SetupTank(m_PlayerAmount);
            ++m_PlayerAmount;
            ++m_RemainingPlayers;
            SetCameraTargets();
            HandleCameras();
            Debug.Log($"Player {m_PlayerAmount} joined the battle!");
        }

        public void OnPlayerLeft(PlayerInput playerInput)
        {
            Debug.Log(playerInput.gameObject + " left");
        }

        #endregion
    }
}